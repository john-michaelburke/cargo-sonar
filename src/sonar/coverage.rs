use serde::Serialize;
use std::path::PathBuf;

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LineToCover {
    pub line_number: usize,
    pub covered: bool,
    pub branches_to_cover: Option<usize>,
    pub covered_branches: Option<usize>,
}

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct File {
    pub path: PathBuf,
    pub line_to_cover: Vec<LineToCover>,
}
#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Coverage {
    pub version: usize,
    pub file: Vec<File>,
}

impl FromIterator<File> for Coverage {
    fn from_iter<T: IntoIterator<Item = File>>(iter: T) -> Self {
        let files = iter.into_iter().collect();
        Self {
            version: 1,
            file: files,
        }
    }
}
