use crate::{cargo::Lockfile, sonar};
use eyre::{Context, Result};
use serde::Deserialize;
use std::{
    collections::{BTreeMap, BTreeSet},
    fs::File,
    path::PathBuf,
};
use tracing::{debug, info};

const UDEPS_ENGINE: &str = "udeps";

#[derive(Debug)]
enum DependencyType {
    Normal,
    Development,
    Build,
}

impl std::fmt::Display for DependencyType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            DependencyType::Normal => write!(f, "normal"),
            DependencyType::Development => write!(f, "development"),
            DependencyType::Build => write!(f, "build"),
        }
    }
}

#[derive(Debug, Deserialize)]
// ALLOW: Unused fields are part of the deserialized schema from 'cargo-udeps'
#[allow(dead_code)]
struct OutcomeUnusedDeps {
    manifest_path: String,
    normal: BTreeSet<String>,
    development: BTreeSet<String>,
    build: BTreeSet<String>,
}

#[derive(Debug, Deserialize)]
// ALLOW: Unused fields are part of the deserialized schema from 'cargo-udeps'
#[allow(dead_code)]
struct Outcome {
    success: bool,
    unused_deps: BTreeMap<String, OutcomeUnusedDeps>,
    note: Option<String>,
}

pub struct Udeps<'lock> {
    json: PathBuf,
    lockfile: &'lock Lockfile,
}

fn extract_package_name(package_id: &str) -> Option<String> {
    package_id
        .split_ascii_whitespace()
        .next()
        .map(str::to_owned)
}

impl<'lock> Udeps<'lock> {
    pub fn new<P>(json: P, lockfile: &'lock Lockfile) -> Self
    where
        P: Into<PathBuf>,
    {
        Self {
            json: json.into(),
            lockfile,
        }
    }

    #[allow(clippy::needless_pass_by_value)]
    fn to_issues(&self, outcome: Outcome) -> sonar::Issues {
        if outcome.success {
            return sonar::Issues::default();
        }
        outcome
            .unused_deps
            .iter()
            .filter_map(|(package_id, outcome_unused)| {
                extract_package_name(package_id).map(|package_name| {
                    outcome_unused
                        .normal
                        .iter()
                        .map(|normal_dep| (DependencyType::Normal, normal_dep))
                        .chain(
                            outcome_unused.development.iter().map(|development_dep| {
                                (DependencyType::Development, development_dep)
                            }),
                        )
                        .chain(
                            outcome_unused
                                .build
                                .iter()
                                .map(|build_dep| (DependencyType::Build, build_dep)),
                        )
                        .map(move |(dep_type, dep)| {
                            debug!(
                                "{} dependency '{}' found in package '{}'",
                                dep_type, dep, package_name
                            );
                            let message = format!(
                                "Dependency '{}' is unused as a {} dependency in package '{}'",
                                dep, dep_type, package_name,
                            );
                            let file_path =
                                self.lockfile.lockfile_path.to_string_lossy().to_string();
                            let text_range = self.lockfile.dependency_range(dep);
                            let primary_location = sonar::Location {
                                message,
                                file_path,
                                text_range,
                            };
                            sonar::Issue {
                                engine_id: UDEPS_ENGINE.to_owned(),
                                rule_id: format!("{}::{}::{}", UDEPS_ENGINE, dep_type, dep),
                                severity: sonar::Severity::Info,
                                r#type: sonar::Type::CodeSmell,
                                primary_location,
                                secondary_locations: Vec::new(),
                            }
                        })
                })
            })
            .flatten()
            .collect()
    }
}

impl std::convert::TryInto<sonar::Issues> for Udeps<'_> {
    type Error = eyre::Error;

    fn try_into(self) -> Result<sonar::Issues> {
        let file = File::open(&self.json).with_context(|| {
            format!(
                "failed to open 'cargo-udeps' report from '{:?}' file",
                self.json
            )
        })?;
        let outcome = serde_json::from_reader::<_, Outcome>(file)
            .context("failed to be parsed as a 'rustsec::report::Report'")?;
        let issues = self.to_issues(outcome);
        info!("{} sonar issues created", issues.len());
        Ok(issues)
    }
}
